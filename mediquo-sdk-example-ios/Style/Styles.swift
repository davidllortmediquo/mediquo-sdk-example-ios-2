//
//  Styles.swift
//  mediquo-sdk-example-ios
//
//  Created by David Martin on 22/2/21.
//

import MediQuo_Base

class Styles {
    static var style: MediQuoStyleType {
        var style = MediQuoStyle()
        style.prefersLargeTitles = true
        style.primaryColor = UIColor(hex: "#6200EEFF")!
        style.primaryContrastColor = .white
        style.secondaryColor = UIColor(hex: "#9952FDFF")!
        style.accentColor = UIColor(hex: "#03DAC5FF")!
        return style
    }
}
