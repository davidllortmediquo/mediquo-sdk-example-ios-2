//
//  AppDelegate.swift
//  mediquo-sdk-example-ios
//
//  Created by David Martin on 20/2/21.
//

import Firebase
import FirebaseMessaging
import MediQuo_Base
import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self

        MediQuo.initialize(apiKey: API_KEY, style: Styles.style) { result in
            if case .success = result {
                CoreLog.business.info("Initialize has been done successfully")
            }
            if case let .failure(error) = result {
                CoreLog.business.info("Initialize has errors %@", error.description)
            }
        }
        return true
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        guard let fcmToken = fcmToken else { return }
        MediQuo.registerFirebase(token: fcmToken) { result in
            if result {
                CoreLog.firebase.info("Firebase registration token: %@", fcmToken)
            } else {
                CoreLog.firebase.error("Can't register Firebase registration token")
            }
        }
    }
}

extension AppDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        if let fcmToken = Messaging.messaging().fcmToken {
            CoreLog.firebase.error("didRegisterForRemoteNotificationsWithDeviceToken: %@", fcmToken)
            MediQuo.registerFirebase(token: fcmToken) { result in
                if result {
                    CoreLog.firebase.info("Firebase registration token: %@", fcmToken)
                } else {
                    CoreLog.firebase.error("Can't register Firebase registration token")
                }
            }
        }
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        CoreLog.firebase.error("didFailToRegisterForRemoteNotificationsWithError: %@", error.localizedDescription)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        MediQuo.didReceiveRemoteNotification(userInfo: userInfo, completion: completionHandler)
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        MediQuo.didReceiveRemoteNotification(userInfo: userInfo, completion: completionHandler)
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        MediQuo.willPresentRemoteNotification(userInfo: userInfo, completion: completionHandler)
    }
}
